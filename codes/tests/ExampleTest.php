<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/test');

        $this->assertEquals(200, $this->response->status());

        $this->assertEquals(
            'Hello', $this->response->getContent()
        );
    }
}
