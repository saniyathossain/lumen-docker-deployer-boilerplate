ARG REDIS_VERSION=6.0.8
ARG REDIS_IMAGE=redis

FROM ${REDIS_IMAGE}:${REDIS_VERSION}

LABEL maintainer="Ahmedul Haque Abid <a_h_abid@hotmail.com>"

ENV REDIS_PASSWORD "foobared"

COPY ./docker/redis/redis.conf /usr/local/etc/redis/redis.conf

EXPOSE 6379

CMD ["sh", "-c", "exec redis-server /usr/local/etc/redis/redis.conf --requirepass \"$REDIS_PASSWORD\""]
